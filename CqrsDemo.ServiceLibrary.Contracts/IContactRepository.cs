﻿using CqrsDemo.Domain.Entities;

namespace CqrsDemo.ServiceLibrary.Contracts
{
    public interface IContactRepository : IRepository<Contact>
    {}
}
