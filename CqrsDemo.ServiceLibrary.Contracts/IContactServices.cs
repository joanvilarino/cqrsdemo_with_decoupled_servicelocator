﻿using CqrsDemo.ServiceLibrary.Contracts.Commands;

namespace CqrsDemo.ServiceLibrary.Contracts
{
    public interface IContactServices
    {
        IAddContactCommand AddContact(string name, string address);
        IUpdateContactNameCommand UpdateContactName(long id, string newName);
        IUpdateYearlyIncomesCommand UpdateContactYearlyIncomes(long id, decimal yearlyIncomes);
    }
}