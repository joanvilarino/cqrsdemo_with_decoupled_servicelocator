﻿using System.Collections.Generic;

namespace CqrsDemo.ServiceLibrary.Contracts
{
    public interface IWriteContext
    {
        int SaveChanges();
        IEnumerable<T> Set<T>() where T : class;
        T Add<T>(T entity) where T : class;
        T Remove<T>(T entity) where T : class;
    }
}
