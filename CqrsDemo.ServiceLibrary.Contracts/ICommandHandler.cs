﻿using CqrsDemo.ServiceLibrary.Contracts.Commands;

namespace CqrsDemo.ServiceLibrary.Contracts
{
    public interface ICommandHandler<T> where T : ICqrsCommand
    {
        void Execute(T command);
    }
}
