﻿using CqrsDemo.Domain.Entities;

namespace CqrsDemo.ServiceLibrary.Contracts.Commands
{
    public interface IAddContactCommand : IContactCommand
    {
        string Name { get; set; }
        string Address { get; set; }
    }
}
