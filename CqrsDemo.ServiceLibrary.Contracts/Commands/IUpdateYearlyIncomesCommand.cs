﻿using CqrsDemo.Domain.Entities;

namespace CqrsDemo.ServiceLibrary.Contracts.Commands
{
    public interface IUpdateYearlyIncomesCommand : IContactCommand
    {
        long Id { get; set; }
        decimal YearlyIncomes { get; set; }
    }
}