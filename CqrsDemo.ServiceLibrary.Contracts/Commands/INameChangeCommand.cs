﻿using CqrsDemo.Domain.Entities;

namespace CqrsDemo.ServiceLibrary.Contracts.Commands
{
    public interface IUpdateContactNameCommand : IContactCommand
    {
        long Id { get; set; }
        string NewName { get; set; }
    }
}
