﻿using CqrsDemo.Domain.Entities;

namespace CqrsDemo.ServiceLibrary.Contracts.Commands
{
    public interface IContactCommand : ICqrsCommand
    {
        Contact UpdatedContact { get; set; }
    }
}