﻿using CqrsDemo.ServiceLibrary.Contracts.Commands;

namespace CqrsDemo.ServiceLibrary.Contracts
{
    public interface IWriteDispatcher
    {
        TSource Dispatch<TSource>(TSource command) where TSource : ICqrsCommand;
    }
}
