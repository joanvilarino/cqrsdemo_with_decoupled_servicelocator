﻿using System;
using System.Collections.Generic;

namespace CqrsDemo.ServiceLibrary.Contracts
{
    public interface IRepository<T> 
    {
        T Add(T entity);
        T Remove(T entity);
        IEnumerable<T> GetAll();
        T GetOne(Func<T, bool> condition);
    }
}
