﻿using System;
using System.Collections.Generic;
using System.Linq;
using CqrsDemo.Domain.Entities;
using CqrsDemo.ServiceLibrary.Contracts;

namespace CqrsDemo.Infrastructure.Repositories
{
    public class ContactRepository : IContactRepository
    {
        protected IWriteContext Context { get; }

        public ContactRepository(IWriteContext context)
        {
            Context = context;
        }

        public Contact Add(Contact entity)
        {
            Context.Add(entity);
            return entity;
        }

        public IEnumerable<Contact> GetAll()
        {
            return Context.Set<Contact>();
        }

        public Contact GetOne(Func<Contact, bool> condition)
        {
            return Context.Set<Contact>().FirstOrDefault(condition);
        }

        public Contact Remove(Contact entity)
        {
            Context.Remove(entity);
            return entity;
        }

    }
}
