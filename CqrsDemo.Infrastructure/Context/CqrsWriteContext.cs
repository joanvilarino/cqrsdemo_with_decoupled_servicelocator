﻿using System.Collections.Generic;
using System.Data.Entity;
using CqrsDemo.Infrastructure.Models;
using CqrsDemo.ServiceLibrary.Contracts;

namespace CqrsDemo.Infrastructure.Context
{
    public class CqrsWriteContext : DbContext, IWriteContext 
    {
        public CqrsWriteContext(string connString) : base(connString)
        {
#if DEBUG
            Database.SetInitializer(new DropCreateDatabaseAlways<CqrsWriteContext>());
#endif
        }

        public T Add<T>(T entity) where T : class
        {
            base.Set<T>().Add(entity);
            return entity;
        }

        public T Remove<T>(T entity) where T : class
        {
            base.Set<T>().Remove(entity);
            return entity;
        }

        public new IEnumerable<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ContactModel());

            base.OnModelCreating(modelBuilder);
        }


    }
}
