﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using CqrsDemo.Domain.Entities;

namespace CqrsDemo.Infrastructure.Models
{
    internal class ContactModel : EntityTypeConfiguration<Contact>
    {
        public ContactModel()
        {
            this.HasKey(t => t.Id);
            this.Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.Name).HasMaxLength(50).IsRequired();
            this.Property(t => t.Address).HasMaxLength(100);
            this.Property(t => t.YearIncomes).HasPrecision(9, 2);
            this.Property(t => t.Active).HasColumnAnnotation("ByActive", new IndexAttribute());
            this.Property(t => t.CreatedOn).HasColumnAnnotation("ByCreatedOn", new IndexAttribute());
        }
    }
}
