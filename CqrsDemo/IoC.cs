﻿using System;
using CqrsDemo.ServiceLocator.Contracts;
using CqrsDemo.ServiceLocator.Contracts.Enums;
using Ninject;

namespace CqrsDemo
{
    public class IoC : IServiceLocator
    {

        private static IoC _instance;
        public static IoC Instance => _instance ?? (_instance = new IoC());

        private readonly IKernel _kernel;

        public IoC()
        {
            _kernel = new StandardKernel();
        }

        public void Register<TInterface, TClass>(ServiceLocatorScopeEnum scope = ServiceLocatorScopeEnum.PerThread)
        {
            var bind =_kernel.Bind(typeof(TInterface)).To(typeof(TClass));
            if (scope == ServiceLocatorScopeEnum.Singleton)
                bind.InSingletonScope();
        }

        public void Register<TSelfClass>(ServiceLocatorScopeEnum scope = ServiceLocatorScopeEnum.PerThread)
        {
        }

        public void Register<TInterface, TClass>(Func<TClass> ctorFunction, ServiceLocatorScopeEnum scope = ServiceLocatorScopeEnum.PerThread)
        {
            var bind = _kernel.Bind(typeof(TInterface)).ToMethod(_ => ctorFunction());
            if (scope == ServiceLocatorScopeEnum.Singleton)
                bind.InSingletonScope();
        }

        public TSource GetInstance<TSource>()
        {
            return _kernel.Get<TSource>();
        }

    }
}
