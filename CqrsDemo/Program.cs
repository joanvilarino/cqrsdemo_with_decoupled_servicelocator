﻿namespace CqrsDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            AppInit.Init(IoC.Instance);
            IoC.Instance.GetInstance<TestClass>().RunTest();
        }
    }
}
