﻿using System;
using System.Collections.Generic;
using System.Linq;
using CqrsDemo.Domain.Entities;
using CqrsDemo.ServiceLibrary.Contracts;
using CqrsDemo.ServiceLibrary.Contracts.Commands;

namespace CqrsDemo
{
    public class TestClass
    {

        private static List<string> _testDataNames = new List<string>
        {
            "Joan",
            "Andreu",
            "Manel",
            "Marc",
            "Jopep",
            "Francesc",
            "Laia",
            "Maria",
            "Anna",
            "Mireia"
        };

        private static List<string> _testDataStreets = new List<string>
        {
            "Mallorca",
            "Aragó",
            "Diagonal",
            "Travessera de les Corts",
            "Rocafort",
            "Urgell",
            "Valencia",
            "Tarragona",
            "Badalona",
            "Hospital"
        };

        private static Random _seed = new Random((int)DateTime.Now.Ticks);
        private IWriteDispatcher Dispatcher { get; }
        private IContactServices ContactServices { get; }

        public TestClass(IWriteDispatcher dispatcher, IContactServices contactServices)
        {
            Dispatcher = dispatcher;
            ContactServices = contactServices;
        }

        public void RunTest()
        {
            _testDataNames.ForEach(n => DispatchCommand(ContactServices.AddContact(n, GetRandomAddress())));
            
            //var newContact = DispatchCommand(ContactServices.AddContact("Joan Vilariño", "C. My Street, 120 - Barcelona"));
            //var updatedContact = DispatchCommand(ContactServices.UpdateContactName(newContact.Id, "Joan Vilariño Abad"));
            //updatedContact = DispatchCommand(ContactServices.UpdateContactYearlyIncomes(updatedContact.Id, 20000));
        }

        private Contact DispatchCommand<T>(T contactCommand) where T : IContactCommand
        {
            var result = Dispatcher.Dispatch(contactCommand);
            return result?.UpdatedContact;
        }

        private string GetRandomAddress()
        {
            var position = _seed.Next(_testDataStreets.Count);
            var street = _testDataStreets.Skip(position).First();
            return $"C. {street}, {_seed.Next(600) + 1}";
        }

    }
}
