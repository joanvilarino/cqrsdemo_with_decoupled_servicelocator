﻿using System;
using System.Configuration;
using System.IO;
using CqrsDemo.Infrastructure.Context;
using CqrsDemo.Infrastructure.Repositories;
using CqrsDemo.ServiceLibrary.Commands;
using CqrsDemo.ServiceLibrary.Contracts;
using CqrsDemo.ServiceLibrary.Contracts.Commands;
using CqrsDemo.ServiceLibrary.Dispatchers;
using CqrsDemo.ServiceLibrary.Handlers;
using CqrsDemo.ServiceLibrary.Services;
using CqrsDemo.ServiceLocator.Contracts;
using CqrsDemo.ServiceLocator.Contracts.Enums;

namespace CqrsDemo
{
    public static class AppInit
    {

        private static IServiceLocator _serviceLocator { get; set; }

        public static void Init(IServiceLocator serviceLocator)
        {
            _serviceLocator = serviceLocator;
            SetAppDataFolder();
            RegisterClasses();
            RegisterLogger();
        }

        static void SetAppDataFolder(string subFolder = "")
        {
            // The database will be created at bin/Debug/ by default
            AppDomain.CurrentDomain.SetData("DataDirectory", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, subFolder));

        }

        static string GetDBConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["default"].ConnectionString;
        }

        static void RegisterClasses()
        {

            // Inject the injector (the singleton instance gets loaded when you use it first time, so it's now
            // after that we will return the instance everytime it needs to be injected or located
            _serviceLocator.Register<IServiceLocator, IServiceLocator>(() => _serviceLocator, ServiceLocatorScopeEnum.Singleton);

            // Commands
            _serviceLocator.Register<IAddContactCommand, AddContactCommand>();
            _serviceLocator.Register<IUpdateContactNameCommand, UpdateContactNameCommand>();
            _serviceLocator.Register<IUpdateYearlyIncomesCommand, UpdateYearlyIncomesCommand>();

            // Command Handlers
            _serviceLocator.Register<ICommandHandler<IAddContactCommand>, AddContactCommandHandler>();
            _serviceLocator.Register<ICommandHandler<IUpdateContactNameCommand>, UpdateContactNameCommandHandler>();
            _serviceLocator.Register<ICommandHandler<IUpdateYearlyIncomesCommand>, UpdateYearlyIncomesCqrsCommandHanlder>();

            // Dispatchers
            _serviceLocator.Register<IWriteDispatcher, WriteDispatcher>();

            // Repositories
            _serviceLocator.Register<IContactRepository, ContactRepository>();

            // Context
            _serviceLocator.Register<IWriteContext, CqrsWriteContext>(() => new CqrsWriteContext(GetDBConnectionString()), 
                ServiceLocatorScopeEnum.Singleton);

            // ServiceLibrary
            _serviceLocator.Register<IContactServices, ContactServices>();

            // Test Class
            _serviceLocator.Register<TestClass>();

        }

        static void RegisterLogger()
        {
            // TODO: Here you can register your logger of choice
        }

    }
}
