﻿using CqrsDemo.ServiceLibrary.Contracts;
using CqrsDemo.ServiceLibrary.Contracts.Commands;
using CqrsDemo.ServiceLocator.Contracts;

namespace CqrsDemo.ServiceLibrary.Services
{
    public class ContactServices : IContactServices
    {

        protected IWriteDispatcher WriteDispatcher { get; }
        protected IServiceLocator IoC { get; }

        public ContactServices(IWriteDispatcher writeDispatcher, IServiceLocator serviceLocator)
        {
            WriteDispatcher = writeDispatcher;
            IoC = serviceLocator;
        }

        public IAddContactCommand AddContact(string name, string address)
        {
            var addCommand = IoC.GetInstance<IAddContactCommand>();
            addCommand.Name = name;
            addCommand.Address = address;
            return addCommand;
        }

        public IUpdateContactNameCommand UpdateContactName(long id, string newName)
        {
            var updateCommand = IoC.GetInstance<IUpdateContactNameCommand>();
            updateCommand.Id = id;
            updateCommand.NewName = newName;
            return updateCommand;
        }

        public IUpdateYearlyIncomesCommand UpdateContactYearlyIncomes(long id, decimal yearlyIncomes)
        {
            var updateCommand = IoC.GetInstance<IUpdateYearlyIncomesCommand>();
            updateCommand.Id = id;
            updateCommand.YearlyIncomes = yearlyIncomes;
            return updateCommand;
        }

    }
}
