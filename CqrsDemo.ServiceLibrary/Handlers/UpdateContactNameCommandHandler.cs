﻿using System;
using System.Linq;
using CqrsDemo.ServiceLibrary.Contracts;
using CqrsDemo.ServiceLibrary.Contracts.Commands;

namespace CqrsDemo.ServiceLibrary.Handlers
{
    public class UpdateContactNameCommandHandler : BaseContactCommandHandler, ICommandHandler<IUpdateContactNameCommand>
    {

        public UpdateContactNameCommandHandler(IContactRepository contactRepository) : base(contactRepository)
        {}

        public void Execute(IUpdateContactNameCommand command)
        {
            var entity = ContactRepository.GetOne(c => c.Id == command.Id);
            if (entity != null)
            {
                entity.Name = command.NewName;
                entity.UpdatedOn = DateTime.Now;
                command.UpdatedContact = entity;
            }
        }
    }
}
