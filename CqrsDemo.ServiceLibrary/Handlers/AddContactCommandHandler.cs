﻿using System;
using CqrsDemo.Domain.Entities;
using CqrsDemo.ServiceLibrary.Contracts;
using CqrsDemo.ServiceLibrary.Contracts.Commands;

namespace CqrsDemo.ServiceLibrary.Handlers
{
    public class AddContactCommandHandler : BaseContactCommandHandler, ICommandHandler<IAddContactCommand>  
    {
        public AddContactCommandHandler(IContactRepository contactRepository) : base(contactRepository) { }

        public void Execute(IAddContactCommand command)
        {
            var newRecord = new Contact
            {
                Name = command.Name,
                Address = command.Address,
                CreatedOn = DateTime.Now,
                Active = true
            };
            ContactRepository.Add(newRecord);
            command.UpdatedContact = newRecord;
        }
    }
}
