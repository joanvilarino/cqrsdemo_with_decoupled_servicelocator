﻿using System;
using CqrsDemo.ServiceLibrary.Contracts;
using CqrsDemo.ServiceLibrary.Contracts.Commands;

namespace CqrsDemo.ServiceLibrary.Handlers
{
    public class UpdateYearlyIncomesCqrsCommandHanlder : BaseContactCommandHandler, ICommandHandler<IUpdateYearlyIncomesCommand>
    {
        public UpdateYearlyIncomesCqrsCommandHanlder(IContactRepository contactRepository) : base(contactRepository)
        { }

        public void Execute(IUpdateYearlyIncomesCommand command)
        {
            var entity = ContactRepository.GetOne(c => c.Id == command.Id);
            if (entity != null)
            {
                entity.YearIncomes = command.YearlyIncomes;
                entity.UpdatedOn = DateTime.Now;
                command.UpdatedContact = entity;
            }
        }

    }
}
