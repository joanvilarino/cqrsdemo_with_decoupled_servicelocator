﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CqrsDemo.ServiceLibrary.Contracts;

namespace CqrsDemo.ServiceLibrary.Handlers
{
    public class BaseContactCommandHandler
    {
        protected IContactRepository ContactRepository { get; }

        public BaseContactCommandHandler(IContactRepository contactRepository)
        {
            ContactRepository = contactRepository;
        }

    }
}
