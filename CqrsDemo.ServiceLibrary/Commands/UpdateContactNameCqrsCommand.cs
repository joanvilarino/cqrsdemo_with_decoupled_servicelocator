﻿using CqrsDemo.Domain.Entities;
using CqrsDemo.ServiceLibrary.Contracts.Commands;

namespace CqrsDemo.ServiceLibrary.Commands
{
    public class UpdateContactNameCommand : IUpdateContactNameCommand
    {
        public long Id { get; set; }
        public string NewName { get; set; }

        public Contact UpdatedContact { get; set; }
    }
}
