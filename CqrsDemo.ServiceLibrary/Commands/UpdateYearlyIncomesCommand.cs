﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CqrsDemo.Domain.Entities;
using CqrsDemo.ServiceLibrary.Contracts.Commands;

namespace CqrsDemo.ServiceLibrary.Commands
{
    public class UpdateYearlyIncomesCommand : IUpdateYearlyIncomesCommand
    {
        public long Id { get; set; }
        public decimal YearlyIncomes { get; set; }
        public Contact UpdatedContact { get; set; }
    }
}
