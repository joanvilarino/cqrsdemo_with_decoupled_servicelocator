﻿using CqrsDemo.Domain.Entities;
using CqrsDemo.ServiceLibrary.Contracts.Commands;

namespace CqrsDemo.ServiceLibrary.Commands
{
    public class AddContactCommand : IAddContactCommand
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public long ContactId { get; set; }

        public Contact UpdatedContact { get; set; }
    }
}
