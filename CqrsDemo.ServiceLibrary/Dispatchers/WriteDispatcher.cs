﻿using CqrsDemo.ServiceLibrary.Contracts;
using CqrsDemo.ServiceLibrary.Contracts.Commands;
using CqrsDemo.ServiceLocator.Contracts;

namespace CqrsDemo.ServiceLibrary.Dispatchers
{
    public class WriteDispatcher : IWriteDispatcher
    {

        protected IWriteContext Context { get; }
        protected IServiceLocator IoC { get; set; }

        public WriteDispatcher(IWriteContext context, IServiceLocator serviceLocator)
        {
            Context = context;
            IoC = serviceLocator;
        }

        public TCommand Dispatch<TCommand>(TCommand command) where TCommand : ICqrsCommand
        {
            var handler = IoC.GetInstance<ICommandHandler<TCommand>>();
            handler.Execute(command);
            Context.SaveChanges();
            return command;
        }
    }
}
