﻿namespace CqrsDemo.ServiceLocator.Contracts.Enums
{
    public enum ServiceLocatorScopeEnum
    {
        PerThread,
        Singleton
    }
}
