﻿using System;
using CqrsDemo.ServiceLocator.Contracts.Enums;

namespace CqrsDemo.ServiceLocator.Contracts
{
    public interface IServiceLocator
    {
        void Register<TInterface, TClass>(ServiceLocatorScopeEnum scope = ServiceLocatorScopeEnum.PerThread);
        void Register<TSelfClass>(ServiceLocatorScopeEnum scope = ServiceLocatorScopeEnum.PerThread);
        void Register<TInterface, TClass>(Func<TClass> ctorFunction, ServiceLocatorScopeEnum scope = ServiceLocatorScopeEnum.PerThread);
        TSource GetInstance<TSource>();
    }
}
